﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace vsacadef6.Models
{
    public class Alumno
    {
        [Key]
        public int AlumnoId { get; set; }
        [Required]
        public string Nombre { get; set; }



        public virtual ICollection<AlumnoCurso> AlumnosCursos { get; set; }

    }
}
