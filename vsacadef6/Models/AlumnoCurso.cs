﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace vsacadef6.Models
{
    public class AlumnoCurso
    {
        public int CursoId { get; set; }
        public int AlumnoId { get; set; }
        public Alumno Alumno { get; set; }
        public Curso Curso { get; set; }

    }
}
