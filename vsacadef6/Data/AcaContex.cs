﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace vsacadef6.Models
{
    public class AcaContex : DbContext
    {
       protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=vsacadef6;Trusted_Connection=True;MultipleActiveResultSets=true");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AlumnoCurso>().HasKey(sc => new { sc.AlumnoId, sc.CursoId });
        }
        public DbSet<vsacadef6.Models.Alumno> Alumno { get; set; }

        public DbSet<vsacadef6.Models.Curso> Curso { get; set; }

        public DbSet<vsacadef6.Models.AlumnoCurso> AlumnoCurso { get; set; }
    }
}
