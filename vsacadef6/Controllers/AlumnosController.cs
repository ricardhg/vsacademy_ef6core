﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using vsacadef6.Models;

namespace vsacadef6.Controllers
{
    public class AlumnosController : Controller
    {
        private readonly AcaContex _context;

        public AlumnosController(AcaContex context)
        {
            _context = context;
        }

        // GET: Alumnos
        public async Task<IActionResult> Index()
        {
            return View(await _context.Alumno.ToListAsync());
        }

        // GET: Alumnos/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var alumno = await _context.Alumno
                .Include(i => i.AlumnosCursos).ThenInclude(i => i.Curso)
                .FirstOrDefaultAsync(m => m.AlumnoId == id);
            if (alumno == null)
            {
                return NotFound();
            }

            var cursos = await _context.Curso.ToListAsync();
            ViewData["ModelCursos"] = cursos;

            return View(alumno);
        }

        // GET: Alumnos/Create
        public async Task<IActionResult> Create()
        {
            var cursos = await _context.Curso.ToListAsync();
            ViewData["ModelCursos"] = cursos;
            return View();
        }


        // POST: Alumnos/NuevoCurso
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> NuevoCurso(int alumnoId, int cursoId)
        {
            if (ModelState.IsValid)
                if (ModelState.IsValid)
                {
                    var curso = _context.Curso.Single(p => p.CursoId == cursoId);
                    var alumno = _context.Alumno.Single(p => p.AlumnoId == alumnoId);

                    var alc = new AlumnoCurso { Alumno = alumno, Curso= curso};

                    _context.AlumnoCurso.Add(alc);

                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Details), new { id = alumnoId });
                }
            return RedirectToAction(nameof(Index));
        }



        // POST: Alumnos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("AlumnoId,Nombre")] Alumno alumno, int cursoid)
        {
            if (ModelState.IsValid)
                if (ModelState.IsValid)
                {
                    var curso = _context.Curso.Single(p => p.CursoId == cursoid);

                    var alc = new AlumnoCurso { Alumno = alumno, Curso = curso };
                    alumno.AlumnosCursos = new List<AlumnoCurso>();
                    alumno.AlumnosCursos.Add(alc);

                    _context.Alumno.Add(alumno);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
            return View(alumno);
        }

        // GET: Alumnos/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var alumno = await _context.Alumno.FindAsync(id);
            if (alumno == null)
            {
                return NotFound();
            }
            return View(alumno);
        }

        // POST: Alumnos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("AlumnoId,Nombre")] Alumno alumno)
        {
            if (id != alumno.AlumnoId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(alumno);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AlumnoExists(alumno.AlumnoId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(alumno);
        }

        // GET: Alumnos/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var alumno = await _context.Alumno
                .FirstOrDefaultAsync(m => m.AlumnoId == id);
            if (alumno == null)
            {
                return NotFound();
            }

            return View(alumno);
        }

        // POST: Alumnos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var alumno = await _context.Alumno.FindAsync(id);
            _context.Alumno.Remove(alumno);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        
        public async Task<IActionResult> DeleteCurso(int idCurso, int idAlumno)
        {
             AlumnoCurso ac = await _context.AlumnoCurso.FindAsync(idAlumno, idCurso);
            _context.AlumnoCurso.Remove(ac);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Details), new { id = idAlumno });
        }




        private bool AlumnoExists(int id)
        {
            return _context.Alumno.Any(e => e.AlumnoId == id);
        }
    }
}


