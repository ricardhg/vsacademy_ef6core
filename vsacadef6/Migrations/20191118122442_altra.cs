﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace vsacadef6.Migrations
{
    public partial class altra : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Idcurso",
                table: "Alumno",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Idcurso",
                table: "Alumno");
        }
    }
}
